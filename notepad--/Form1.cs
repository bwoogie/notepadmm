﻿using System;
using System.Diagnostics;
using System.IO;
using System.Runtime.InteropServices;
using System.Windows.Forms;

namespace notepad__ {
    public partial class Form1 : Form {

        [DllImport("user32.dll")]
        static extern bool CreateCaret(IntPtr hWnd, IntPtr hBitmap, int nWidth, int nHeight);
        [DllImport("user32.dll")]
        static extern bool ShowCaret(IntPtr hWnd);

        private string file;
        private bool dirty = false;

        public Form1() {
            InitializeComponent();
            newFile();

            textBox1.GotFocus += textbox1_GotFocus;

            String[] args = Environment.GetCommandLineArgs();
            if (args != null && args.Length > 0) {

                foreach (String arg in args) {
                    checkFileAndOpen(arg);
                }

                String[] actdata = AppDomain.CurrentDomain.SetupInformation.ActivationArguments.ActivationData;
                if (actdata != null) {
                foreach (String arg in actdata) {
                        checkFileAndOpen(arg);
                }
            }

            }
        }

        private void checkFileAndOpen(String file) {
            if (File.Exists(file)) {
                if (Path.GetExtension(file).ToLower() == ".txt")
                    openFile(file);
            }
        }

        private void textbox1_GotFocus(object sender, EventArgs e) {
            createCaret();
        }

        private void createCaret() {
            CreateCaret(textBox1.Handle, IntPtr.Zero, 10, textBox1.Height);
            ShowCaret(textBox1.Handle);
        }

        private void newFile() {
            file = "";
            textBox1.Text = "";
            makeClean();
        }

        private void openFile(string file) {
            try {
                using (StreamReader sr = new StreamReader(file)) {
                    String line = sr.ReadToEnd();
                    line = line.Replace(System.Environment.NewLine, " ");
                    textBox1.Text = line;
                    this.file = file;
                    makeClean();
                }
            } catch (Exception e) {
                Console.WriteLine("The file could not be read:");
                Console.WriteLine(e.Message);
            }
        }

        private void openFile() {
            OpenFileDialog ofd = new OpenFileDialog();
            ofd.Filter = "Text Files (*.txt)|*.txt";
            if(ofd.ShowDialog() == DialogResult.OK) {
                openFile(ofd.FileName);
            }
        }

        private void saveFile() {
            saveFile(false);
        }

        private void saveFile(bool forceDialog) {
            if(file == "" || forceDialog) {
                SaveFileDialog sfd = new SaveFileDialog();
                sfd.Filter = "Text Files (*.txt)|*.txt";
                if (sfd.ShowDialog() == DialogResult.OK) {
                    File.WriteAllText(sfd.FileName, textBox1.Text);
                    file = sfd.FileName;
                    makeClean();
                }
            } else {
                File.WriteAllText(file, textBox1.Text);
                makeClean();
            }
        }

        private void makeDirty() {
            dirty = true;
            updateTitle();
        }

        private void makeClean() {
            dirty = false;
            updateTitle();
        }

        private void updateTitle() {
            this.Text = (file == "" ? "Untitled" : Path.GetFileName(file)) + (dirty ? "*" : "") + " - " + Application.ProductName;
        }

        private void openToolStripMenuItem_Click(object sender, EventArgs e) {
            openFile();
        }

        private void newToolStripMenuItem_Click(object sender, EventArgs e) {
            newFile();
        }

        private void textBox1_TextChanged(object sender, EventArgs e) {
            makeDirty();
        }

        private void saveToolStripMenuItem_Click(object sender, EventArgs e) {
            saveFile();
        }

        private void saveAsToolStripMenuItem_Click(object sender, EventArgs e) {
            saveFile(true);
        }

        private void Form1_Shown(object sender, EventArgs e) {
        }

        private void aboutToolStripMenuItem_Click(object sender, EventArgs e) {
            MessageBox.Show("Written by bwoogie");
        }

    }
}
